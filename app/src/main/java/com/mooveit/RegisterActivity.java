package com.mooveit;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    //EditTexts initialization
    private EditText username;
    private EditText email;
    private EditText password;
    private EditText passwordConfirm;

    //Initialization of the TextView appearing on register success or error
    private TextView registerResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Bindings
        username = (EditText) findViewById(R.id.username_register_field);
        email = (EditText) findViewById(R.id.email_register_field);
        password = (EditText) findViewById(R.id.password_register_field);
        passwordConfirm = (EditText) findViewById(R.id.password_confirm_field);

        registerResult = (TextView) findViewById(R.id.register_result);

        //Buttons initialization
        Button registerBtn = findViewById(R.id.bt_register);
        Button goBackBtn = findViewById(R.id.bt_go_back_register);

        //Buttons activation
        registerBtn.setOnClickListener(this);
        goBackBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_register :
                //hide information text on new attempts
                registerResult.setVisibility(View.GONE);

                //Get the content of EditTexts
                final String username_string = username.getText().toString();
                final String email_string = email.getText().toString();
                final String password_string = password.getText().toString();
                final String passwordConfirm_string = passwordConfirm.getText().toString();

                //Compare the passwords fields values, exit instantly if not equal
                if (!passwordConfirm_string.equals(password_string)){
                    setLayout(1);
                    break;
                }

                //Check that fields are not empty
                if (username_string.equals("") || email_string.equals("") || password_string.equals("")){
                    setLayout(3);
                    break;
                }

                /* POST REQUEST TO INSERT THE USER IN THE DATABASE*/
                // Instantiate the RequestQueue.
                RequestQueue queue = Volley.newRequestQueue(this);
                String url ="http://192.168.43.95:3001/api/auth/signup";

                // Send the gathered data the provided URL.
                Map<String, String> params = new HashMap<>();
                params.put("username", username_string);
                params.put("email", email_string);
                params.put("password", password_string);
                JSONObject parameters = new JSONObject(params);

                JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        setLayout(0);
                        Log.i("VOLLEY", response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        setLayout(2);
                        Log.i("VOLLEY", error.toString());
                    }
                });

                queue.add(jsonRequest);

                break;

            case R.id.bt_go_back_register :
                finish();
                break;
        }
    }


    // This function updates the layout depending on the result of the register attempt
    private void setLayout(int code){
        switch (code){
            //success
            case 0:
                username.setText("");
                email.setText("");
                password.setText("");
                passwordConfirm.setText("");
                registerResult.setText(R.string.register_success);
                registerResult.setTextColor(getResources().getColor(R.color.colorSuccess));
                registerResult.setVisibility(View.VISIBLE);
                break;
            //passwords not equal
            case 1:
                passwordConfirm.setText("");
                registerResult.setText(R.string.register_pwd_error);
                registerResult.setTextColor(getResources().getColor(R.color.colorError));
                registerResult.setVisibility(View.VISIBLE);

                break;
            //Already taken or response error
            case 2:
                username.setText("");
                email.setText("");
                password.setText("");
                passwordConfirm.setText("");
                registerResult.setText("       Something's wrong \n Consider trying other values");
                registerResult.setTextColor(getResources().getColor(R.color.colorError));
                registerResult.setVisibility(View.VISIBLE);
                break;
            case 3:
                username.setText("");
                email.setText("");
                password.setText("");
                passwordConfirm.setText("");
                registerResult.setText(R.string.register_credentials_error);
                registerResult.setTextColor(getResources().getColor(R.color.colorError));
                registerResult.setVisibility(View.VISIBLE);

        }
    }
}
