package com.mooveit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.auth0.android.jwt.JWT;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    private Button joinBtn;
    //Textview used to display the username
    private TextView usernameDisplay;
    //Textview used to show the code obtained with the QR scanner (roomId)
    private  String roomId;
    private TextView codeDisplay;
    //Launch code of the Login activity
    static final int GETUSERNAME = 1;
    //Launch code of the QR code scanner activity
    static final int GETCODE = 2;
    //Socket needs
    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://192.168.43.95:3001?type=mobileClient"); //107
        } catch (URISyntaxException ignored) {}
    }

    private SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //TextViews binding
        usernameDisplay = findViewById(R.id.username);
        registerForContextMenu(usernameDisplay);
        codeDisplay = findViewById(R.id.code_scanned);

        //Buttons binding & activation
        Button launchActivityBtn = findViewById(R.id.bt_launch_scanner);
        joinBtn = findViewById(R.id.bt_join);
        joinBtn.setVisibility(View.INVISIBLE);

        launchActivityBtn.setOnClickListener( this );
        joinBtn.setOnClickListener( this );

        Button testBtn = findViewById(R.id.bt_test);
        testBtn.setOnClickListener(this);

        //Persistent session management check
        sharedpreferences = getSharedPreferences(LoginActivity.myPreferences, Context.MODE_PRIVATE);
        System.out.println(sharedpreferences.toString());

        String username = sharedpreferences.getString("nameOrEmailKey", null);
        String token = sharedpreferences.getString("token", null);
        if (username!=null && token!=null){

            JWT jwt = new JWT(token);
            if (jwt.isExpired(10)) { //Token expired redirect to Main Menu.
                launchLoginActivity();
            } else {
                usernameDisplay.setText(username);
            }
        }
        else { // If there is no user found in cookies, launch LoginActivity
            launchLoginActivity();
        }

        //Socket storage to transmit it to controller activity
        SocketHandler.setSocket(mSocket);

        //Socket connection
        mSocket.connect();

        //Socket events triggered on backend responses through the socket
        mSocket.on("JOIN_ROOM", onRightCode);
        mSocket.on("ROOM_UNDEFINED", onWrongCode);
    }

    /* BUTTONS CLICKS MANAGEMENT */
    @Override
    public void onClick(final View v) {
        switch (v.getId()){
            case R.id.bt_launch_scanner :
                launchQRCameraActivity();
                break;

            case R.id.bt_join :
                attemptSend();
                break;

            case R.id.bt_test :
                launchControllerActivity("joystick");
                break;
        }
    }

    /* The two functions under are used to create the menu on username click */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(usernameDisplay.getText());
        getMenuInflater().inflate(R.menu.menu_user, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.disconnect:
                sharedpreferences = getSharedPreferences(LoginActivity.myPreferences, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear();
                editor.apply();
                Toast.makeText(this, "Disconnected",Toast.LENGTH_SHORT).show();
                this.recreate();
                return true;
            case R.id.do_nothing:
                Toast.makeText(this, "Thanks !",Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    /* FUNCTIONS LAUNCHING OTHER ACTIVITIES */

    private void launchLoginActivity(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, GETUSERNAME);
    }

    private void launchQRCameraActivity() {
        //Put back display to normal if it was launched previously
        codeDisplay.setText("");
        joinBtn.setVisibility(View.GONE);
        Intent intent = new Intent(this, QRCameraActivity.class);
        startActivityForResult(intent, GETCODE);
    }

    private void launchControllerActivity(String remoteType) {
        Intent intent;
        if (remoteType.equals("joystick")) {
            intent = new Intent(this, JoystickControllerActivity.class);
        }
        else {
            intent = new Intent(this, ControllerActivity.class);
        }
        startActivity(intent);
    }

    /* FUNCTION HANDLING VALUES OBTAINED THROUGH OTHER ACTIVITIES FINISH */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check which request we're responding to
        if (requestCode == GETUSERNAME) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                //The user successfully logged in
                usernameDisplay.setText(data.getStringExtra("USERNAME"));
            }
        }

        if (requestCode == GETCODE) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // The QR scanner picked a code.
                roomId = data.getStringExtra("CODE");
                if (!(roomId != null && roomId.equals(""))){
                    codeDisplay.setText("Code scanned : "+ roomId);
                    joinBtn.setVisibility(View.VISIBLE);
                }
                else{
                    codeDisplay.setText("No code scanned :(");
                }
            }
        }
    }

/*------------------------------------------------------------------------------------------------*/
    /* SOCKET MANAGEMENT SECTION */
    //Function sending the code obtained via QR or typed by the user.
    //The back end checks its veracity and tells whether it is correct or not.
    private void attemptSend() {
        if (TextUtils.isEmpty(roomId)) {
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("roomId", roomId);
        mSocket.emit("JOIN_ROOM", new JSONObject(params));
    }

    //If the code is correct, this function will be called with the mSocket.on (see onCreate)
    private Emitter.Listener onRightCode = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            JSONObject data = (JSONObject) args[0];
            System.out.println("J'ai reçu CEEEEEEEEEEECIIIIIIIIIIIIIIIIIIIII : " + data.toString());
            String remote;
            try {
                remote = data.getString("remote");
            } catch (JSONException e) {
                return;
            }

            launchControllerActivity(remote);

            //The two lines inside need to be wrapped with this to avoid an crash error
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Textview appearing if the code scanned is not compatible
                    joinBtn.setVisibility(View.INVISIBLE);
                    codeDisplay.setText("No code scanned yet");
                }
            });
        }
    };

    //If the code is incorrect, this function will be called with the mSocket.on (see onCreate)
    private Emitter.Listener onWrongCode = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            //The two lines inside need to be wrapped with this to avoid a crash error
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Textview appearing if the code scanned is not compatible
                    TextView invalidCode = findViewById(R.id.invalidCode);
                    invalidCode.setVisibility(View.VISIBLE);
                }
            });
        }
    };

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        mSocket.disconnect();
        super.onDestroy();
    }
}
