package com.mooveit;

public interface KeyCode {
    public String up = "up";
    public String down = "down";
    public String left = "left";
    public String right = "right";
}
