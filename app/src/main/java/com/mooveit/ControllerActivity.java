package com.mooveit;

        import androidx.appcompat.app.AppCompatActivity;

        import android.annotation.SuppressLint;
        import android.content.Context;
        import android.content.Intent;
        import android.hardware.Sensor;
        import android.hardware.SensorEvent;
        import android.hardware.SensorEventListener;
        import android.hardware.SensorManager;
        import android.os.Bundle;
        import android.view.MotionEvent;
        import android.view.View;
        import android.widget.Button;
        import android.widget.ImageButton;
        import android.widget.Toast;

        import com.github.nkzawa.emitter.Emitter;
        import com.github.nkzawa.socketio.client.Socket;

        import org.json.JSONException;
        import org.json.JSONObject;

        import java.util.ArrayList;
        import java.util.Collections;
        import java.util.HashMap;
        import java.util.Map;

        import static com.mooveit.SocketHandler.*;
        import static java.lang.Math.abs;

public class ControllerActivity extends AppCompatActivity implements SensorEventListener, View.OnClickListener, View.OnTouchListener, KeyCode {

    /* SENSORS VARIABLES */
    private SensorManager senSensorManager;
    private Sensor senAccelerometer;        //Acceleration sensor
    private long lastUpdate = 0;

    private ArrayList<Float> arrayX = new ArrayList<>();
    private ArrayList<Float> arrayY = new ArrayList<>();
    private ArrayList<Float> arrayZ = new ArrayList<>();

    /* MEASURES VARIABLES */
    private ArrayList<ArrayList<Float>> measures = new ArrayList<>();       //Used to store data from accelerometer measures

    //Socket creation : retrieves the MainActivity's socket
    private Socket gameSocket = getSocket();

    //Ready Button
    private Button readyBtn;

    //Controls button
    private ImageButton acquireBtn;

    @SuppressLint("ClickableViewAccessibility") //removes warnings on "setOnTouchListener"
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controller);

        //Buttons binding
        acquireBtn = findViewById(R.id.bt_move);

        readyBtn = findViewById(R.id.bt_ready);

        //Quit button
        Button quitBtn = findViewById(R.id.bt_quit);

        //Buttons activation
        acquireBtn.setOnTouchListener(this);

        readyBtn.setOnClickListener(this);
        quitBtn.setOnClickListener(this);

        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = (senSensorManager != null) ? senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) : null;
        if (senSensorManager != null) {
            senSensorManager.registerListener(this, senAccelerometer , SensorManager.SENSOR_DELAY_FASTEST);
        }

        gameSocket.on("CHANGE_REMOTE", onChangeRemote);
    }

    /* BUTTONS CLICKS MANAGEMENT */

    @Override
    public void onClick(final View v) {
        switch (v.getId()){
            case R.id.bt_quit :
                gameSocket.emit("LEAVE");
                finish();
                break;
            case R.id.bt_ready:
                /* This button gives access to the controls and give information that the player is ready */
                Toast.makeText(this, "Game started !",Toast.LENGTH_SHORT).show();
                readyBtn.setVisibility(View.GONE);
                acquireBtn.setVisibility(View.VISIBLE);
                gameSocket.emit("TOGGLE_PLAYER_READY");
                break;
            default:
                break;
        }
    }

    /* BUTTON PRESS FOR MOVEMENT DETECTION  */

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent event){
        if (view.getId() == R.id.bt_move) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    arrayX.clear();
                    arrayY.clear();
                    arrayZ.clear();
                    measures.clear();
                    senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
                    return true;
                case MotionEvent.ACTION_UP:
                    senSensorManager.unregisterListener(this, senAccelerometer);
                    orientation(arrayX, arrayY, arrayZ);
                    return true;
            }
            return false;
        }
        return true;
    }

    /* SENSORS MANAGEMENT */
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;
        long curTime = System.currentTimeMillis();
        long diffTime = (curTime - lastUpdate);     //To know how much time has passed since last measure

        //Process to collect the measures of the accelerometer
        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER && (measures.size() < 100)) {
            if (diffTime >= 10) {    //Check if 10 ms have passed
                float x = sensorEvent.values[0];
                float y = sensorEvent.values[1];
                float z = sensorEvent.values[2] - ((float) 9.81);
                lastUpdate = curTime;

                ArrayList<Float> value = new ArrayList<>();
                arrayX.add(x); arrayY.add(y); arrayZ.add(z);
                value.add(x); value.add(y); value.add(z);
                measures.add(value);
                System.out.println("after " + diffTime + " ms, measure n° : " + measures.size());

            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /* FUNCTION USED IN ORIENTATION() */
    private boolean getSens(ArrayList<Float> vector) {
        boolean sens = false;
        /*Détermination du sens du vecteur
        en observant le signe de la différence entre la dernière et la première valeur*/
        if (vector.get(vector.size() - 10) - vector.get(vector.size() - 5) > 0) {
            sens = true; //on se déplace dans le sens négatif
        }
        return sens;
    }

/*------------------------------------------------------------------------------------------------*/

    /* ALGORITHM TO GET THE MAIN DIRECTION */
    private void orientation(ArrayList<Float> x,ArrayList<Float> y,ArrayList<Float> z) {
        int marge = 5; //Marge de différence de distance tolérée
        String ret = ""; //Initialisation de la chaîne de caractères retournée

        if (x.size() < 12) { // Check that there are enough values in the vector to avoid a crash
            return;
        }

            //calcul des distances parcourues sur chaque axe
        float distX = Collections.max(x) - Collections.min(x);
        float distY = Collections.max(y) - Collections.min(y);
        float distZ = Collections.max(z) - Collections.min(z);

        System.out.println("Distance x : " + distX + " // Distance y : " + distY +  " // Distance z : " + distZ);

        /* Comparaison des distances.La plus grande détermine la direction du geste
        Le sens est déterminé par une fonction externe */
        if (abs(distX) > abs(distY) + marge) { //On compare les distances à une marge près
            if (abs(distX) > abs(distZ) + marge) {
                if (getSens(x)) {
                    ret += "RIGHT";
                } else {
                    ret += "LEFT";
                }
            } else if (abs(distZ) > abs(distX) + marge) {
                if (getSens(z)) {
                    ret += "ABOVE";
                } else {
                    ret += "BELOW";
                }
            }

            // Le mouvement n'est ni prédominant sur l' axe X ni sur l 'axe Z
            else {
                if (getSens(x) && getSens(z)) {
                    ret += "ABOVE-RIGHT";
                } else if (getSens(x)) {
                    ret += "BELOW-RIGHT";
                } else if (getSens(z)) {
                    ret += "ABOVE-LEFT";
                } else {
                    ret += "BELOW-LEFT";
                }
            }
        }
        //Si la distance sur l 'axe Y est considérablement plus grande que celle sur l' axe X
        else if (abs(distX) + marge < abs(distY)) {
            if (abs(distY) > abs(distZ) + marge) {
                if (getSens(y)) {
                    ret += "UP";
                } else {
                    ret += "DOWN";
                }
            } else if (abs(distZ) > abs(distY) + marge) {
                if (getSens(z)) {
                    ret += "ABOVE";
                } else {
                    ret += "BELOW";
                }
            }
            //Le mouvement n 'est ni prédomiant sur l' axe Y ni sur l 'axe Z
            else {
                if (getSens(y) && getSens(z)) {
                    ret += "UP-ABOVE";
                } else if (getSens(z)) {
                    ret += "DOWN-ABOVE";
                } else if (getSens(y)) {
                    ret += "UP-BELOW";
                } else {
                    ret += "DOWN-BELOW";
                }
            }
        }

        //Si les distances sur l 'axe X et Y sont égale à deux marges près
        else if ((abs(distX) + marge > abs(distY)) && (abs(distY) > abs(distX) - marge)) {
            if (abs(distX) < abs(distZ)) {
                if (getSens(z)) {
                    ret += "ABOVE";
                } else {
                    ret += "BELOW";
                }
            } else if (abs(distX) > abs(distZ)) {
                if (getSens(x) && getSens(y)) {
                    ret += "UP-RIGHT";
                } else if (getSens(x)) {
                    ret += "DOWN-RIGHT";
                } else if (getSens(y)) {
                    ret += "UP-LEFT";
                } else {
                    ret += "DOWN-LEFT";
                }
            }
        }


        //Sinon, les 3 distances sont égales à deux marges près
        else {
            if (getSens(x) && getSens(y) && getSens(z)) {
                ret += "RIGHT-UP-ABOVE";
            } else if (getSens(x) && getSens(y)) {
                ret += "RIGHT-UP-BELOW";
            } else if (getSens(x) && getSens(z)) {
                ret += "RIGHT-DOWN-ABOVE";
            } else if (getSens(y) && getSens(z)) {
                ret += "LEFT-UP-ABOVE";
            } else if (getSens(x)) {
                ret += "RIGHT-DOWN-BELOW";
            } else if (getSens(y)) {
                ret += "LEFT-UP-BELOW";
            } else if (getSens(z)) {
                ret += "LEFT-DOWN-ABOVE";
            } else {
                ret += "LEFT-DOWN-BELOW";
            }
        }

        System.out.println("Mouvement détecté : " + ret);
        Map<String, String> params = new HashMap<>();
        params.put("direction", ret);
        params.put("measures", measures.toString());
        gameSocket.emit("MOVE_DETECTED", new JSONObject(params));
    }

    private Emitter.Listener onChangeRemote = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            JSONObject data = (JSONObject) args[0];
            System.out.println("J'ai reçu CEEEEEEEEEEECIIIIIIIIIIIIIIIIIIIII : " + data.toString());
            String remote;
            try {
                remote = data.getString("remote");
            } catch (JSONException e) {
                return;
            }
            launchControllerActivity(remote);
        }
    };

    private void launchControllerActivity(String remoteType) {
        Intent intent;
        if (remoteType.equals("joystick")) {
            intent = new Intent(this, JoystickControllerActivity.class);
        }
        else {
            intent = new Intent(this, ControllerActivity.class);
        }
        finish();
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        senSensorManager.unregisterListener(this, senAccelerometer);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        senSensorManager.unregisterListener(this, senAccelerometer);
    }
}
