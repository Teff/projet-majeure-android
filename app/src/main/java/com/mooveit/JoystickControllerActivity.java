package com.mooveit;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.github.controlwear.virtual.joystick.android.JoystickView;

import static com.mooveit.SocketHandler.*;

public class JoystickControllerActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener, KeyCode {

    //Socket creation : retrieves the MainActivity's socket
    private Socket gameSocket = getSocket();

    //Ready Button
    private Button readyBtn;

    //Arrows buttons
    private ImageButton rightBtn;
    private ImageButton leftBtn;
    private ImageButton upBtn;
    private ImageButton downBtn;

    //Joystick-related TextViews and joystick declaration
    private TextView mTextViewCoordinateLeft;
    private JoystickView joystickRight;

    @SuppressLint("ClickableViewAccessibility") //removes warnings on "setOnTouchListener"
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joystick_controller);

        //Buttons binding
        upBtn = findViewById(R.id.UP);
        downBtn = findViewById(R.id.DOWN);
        leftBtn = findViewById(R.id.LEFT);
        rightBtn = findViewById(R.id.RIGHT);

        readyBtn = findViewById(R.id.bt_ready);
        //Quit button
        Button quitBtn = findViewById(R.id.quit_btn);

        //Buttons activation
        upBtn.setOnTouchListener(this);
        downBtn.setOnTouchListener(this);
        leftBtn.setOnTouchListener(this);
        rightBtn.setOnTouchListener(this);

        readyBtn.setOnClickListener(this);
        quitBtn.setOnClickListener(this);

        // Joystick implementation
        mTextViewCoordinateLeft = findViewById(R.id.textView_coordinate_left);

        joystickRight = findViewById(R.id.joystickView_left);
        joystickRight.setOnMoveListener(new JoystickView.OnMoveListener() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onMove(int angle, int strength) {
                float x = ((float)joystickRight.getNormalizedX()-50) / 50;
                float y = -(((float)joystickRight.getNormalizedY()-50) / 50);

                mTextViewCoordinateLeft.setText(String.format("x : %03f / y : %03f", x, y));

                Map<String, String> params = new HashMap<>();
                params.put("x", String.valueOf(x));
                params.put("y", String.valueOf(y));

                gameSocket.emit("JOYSTICK_MOVE", new JSONObject(params));
            }
        }, 100 ); //every 100 ms

        gameSocket.on("CHANGE_REMOTE", onChangeRemote);
    }

    /* BUTTONS CLICKS MANAGEMENT */
    @Override
    public void onClick(final View v) {
        switch (v.getId()){
            case R.id.quit_btn :
                gameSocket.emit("LEAVE");
                finish();
                break;
            case R.id.bt_ready:
                Toast.makeText(this, "Game started !",Toast.LENGTH_SHORT).show();
                readyBtn.setVisibility(View.GONE);
                upBtn.setVisibility(View.VISIBLE);
                downBtn.setVisibility(View.VISIBLE);
                leftBtn.setVisibility(View.VISIBLE);
                rightBtn.setVisibility(View.VISIBLE);
                joystickRight.setVisibility(View.VISIBLE);
                gameSocket.emit("TOGGLE_PLAYER_READY");
                break;
            default:
                break;
        }
    }

    /* CONTROLS PRESS MANAGEMENT */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent event){
        switch (view.getId()){
            case R.id.UP:
                return directionHandler(event, KeyCode.up);

            case R.id.DOWN :
                return directionHandler(event, KeyCode.down);

            case R.id.LEFT :
                return directionHandler(event, KeyCode.left);

            case R.id.RIGHT :
                return directionHandler(event, KeyCode.right);

            default:
                return false;
        }
    }

    private boolean directionHandler(MotionEvent event, String key){
        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                moveSend(key, "true");
                return true;
            case MotionEvent.ACTION_UP:
                // No longer down
                moveSend(key, "false");
                return true;
        }
        return false;
    }

    // Function sending the instructions to the backend according to the button pressed
    private void moveSend(String keyCode, String value) {
        Map<String, String> params = new HashMap<>();
        params.put("id", keyCode);
        params.put("value",value);
        gameSocket.emit("PRESS_BUTTON", new JSONObject(params));
    }

    private Emitter.Listener onChangeRemote = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            JSONObject data = (JSONObject) args[0];
            System.out.println("J'ai reçu CEEEEEEEEEEECIIIIIIIIIIIIIIIIIIIII : " + data.toString());
            String remote;
            try {
                remote = data.getString("remote");
            } catch (JSONException e) {
                return;
            }
            launchControllerActivity(remote);
        }
    };

    private void launchControllerActivity(String remoteType) {
        Intent intent;
        if (remoteType.equals("joystick")) {
            intent = new Intent(this, JoystickControllerActivity.class);
        }
        else {
            intent = new Intent(this, ControllerActivity.class);
        }
        finish();
        startActivity(intent);
    }

}
