package com.mooveit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    //TextView displayed in case of invalid credentials
    private TextView invalidCreds;
    //EditTexts
    private EditText username_input;
    private EditText password_input;
    //For persistent session
    private SharedPreferences sharedpreferences;
    public static final String myPreferences = "cookies" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //TextViews binding
        //TextView giving link to the register page
        TextView registerLink = findViewById(R.id.link);
        registerLink.setOnClickListener(this);
        invalidCreds = findViewById(R.id.invalidCreds);

        //EditTexts binding
        username_input = (EditText)findViewById(R.id.first_input);
        password_input = (EditText)findViewById(R.id.second_input);

        //Buttons binding & activation
        //Submit button
        Button submitBtn = findViewById(R.id.bt_submit);
        submitBtn.setOnClickListener( this );

        //Continue as guest button
        Button guestBtn = findViewById(R.id.bt_guest);
        guestBtn.setOnClickListener( this );

        //Persistent session management initialization
        sharedpreferences = getSharedPreferences(myPreferences, Context.MODE_PRIVATE);
    }

    /* BUTTONS CLICKS MANAGEMENT */
    @Override
    public void onClick(View v) {
        //Prepare the variable "data" that will hold the value returned to MainActivity
        final Intent data = new Intent();
        switch (v.getId()){
            case R.id.bt_submit :
                System.out.println("TEST");

                final String username = username_input.getText().toString();
                final String password = password_input.getText().toString();

                /* POST REQUEST TO CHECK IF THE USER EXISTS*/
                // Instantiate the RequestQueue.
                RequestQueue queue = Volley.newRequestQueue(this);
                String url ="http://192.168.43.95:3001/api/auth/login";

                // Send the gathered data the provided URL.
                Map<String, String> params = new HashMap<>();
                params.put("usernameOrEmail", username);
                params.put("password", password);
                JSONObject parameters = new JSONObject(params);

                JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, parameters, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("VOLLEY", response.toString());

                        //---set the data to pass back to "Main Activity"---
                        data.putExtra("USERNAME", username);
                        setResult(RESULT_OK, data);
                        //---Edit the preferences for persistent session (tested on launch in main activity)
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("nameOrEmailKey", username);
                        try{
                            String token = response.get("token").toString();
                            editor.putString("token", token);
                        }catch(JSONException e){e.printStackTrace();}
                        editor.apply();
                        //---Login successful : close the activity---
                        finish();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        invalidCreds.setVisibility(View.VISIBLE);
                        username_input.setText("");
                        password_input.setText("");
                        error.printStackTrace();
                        Log.i("VOLLEY", error.toString());
                    }
                });

                queue.add(jsonRequest);

                break;

            case R.id.bt_guest :
                //---set the data to pass back---
                data.putExtra("USERNAME", "Connected as guest");
                setResult(RESULT_OK, data);
                //---close the activity---
                finish();
                break;

            case R.id.link :
                Intent myIntent = new Intent(this, RegisterActivity.class);
                startActivity(myIntent);
                break;
        }
    }


}
