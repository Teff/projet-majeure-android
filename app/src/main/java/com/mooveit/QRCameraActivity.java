package com.mooveit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

public class QRCameraActivity extends AppCompatActivity {

    private CameraSource cameraSource;
    private TextView textView;
    private EditText code_input;

    private SurfaceHolder surfaceHolderCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcamera);

        /* CAMERA PERMISSION ASKING */
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 0);
        }

        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.camera);
        textView = (TextView)findViewById(R.id.cameraHelp);
        code_input = findViewById(R.id.code_input);

        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE).build();

        cameraSource = new CameraSource.Builder(this, barcodeDetector)
                .setRequestedPreviewSize(640,480).build();

        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                surfaceHolderCamera = surfaceHolder;
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                    return;
                }
                try
                {
                    cameraSource.start(surfaceHolder);
                }catch (IOException e)
                {
                    e.printStackTrace();
                }

            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> qrCodes = detections.getDetectedItems();
                if (qrCodes.size() != 0)
                {
                    textView.post(new Runnable() {
                        @Override
                        public void run() {
                            Vibrator vibrator = (Vibrator)getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            if (vibrator != null) {
                                vibrator.vibrate(1000);
                            }
                            String codeString = qrCodes.valueAt(0).displayValue;
                            textView.setText(codeString);

                            //---set the data to pass back---
                            Intent data = new Intent();
                            data.putExtra("CODE",codeString);
                            setResult(RESULT_OK, data);
                            //---close the activity---
                            finish();
                        }
                    });
                }
            }
        });

        Button goBackBtn = findViewById(R.id.bt_go_back);

        goBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //---set the data to pass back---
                Intent data = new Intent();
                data.putExtra("CODE", "");
                setResult(RESULT_OK, data);
                //end activity
                finish();
            }
        });

        Button submit = findViewById(R.id.bt_submit_code);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String codeSent = code_input.getText().toString();

                //---set the data to pass back---
                Intent data = new Intent();
                data.putExtra("CODE", codeSent);
                setResult(RESULT_OK, data);
                //end activity
                finish();
            }
        });
    }

    @SuppressLint("NewApi")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            Toast.makeText(this, "This app needs to access camera to run correctly !",Toast.LENGTH_SHORT).show();
        }else{
            try
            {
                cameraSource.start(surfaceHolderCamera);
            }catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

}
