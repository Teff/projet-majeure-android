package com.mooveit;

import com.github.nkzawa.socketio.client.Socket;

class SocketHandler {
    private static Socket socket;

    static synchronized Socket getSocket(){
        return socket;
    }

    static synchronized void setSocket(Socket socket){
        SocketHandler.socket = socket;
    }
}
